import Pages.RegistrationPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class RegistrationTest {

    WebDriver driver;
    RegistrationPage registrationPage;

    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://account.mail.ru/signup");
    }

    @Test
    public void registrationWithoutFillFields() {
        registrationPage = new RegistrationPage(driver);
        registrationPage.submitRegistration();
        Assertions.assertEquals("Укажите имя", registrationPage.getTextErrorFirstName());
        Assertions.assertEquals("Укажите фамилию", registrationPage.getTextErrorLastName());
        Assertions.assertEquals("Укажите дату рождения", registrationPage.getTextErrorDOB());
        Assertions.assertEquals("Укажите ваш пол", registrationPage.getTextErrorSex());
        Assertions.assertEquals("Укажите желаемое имя аккаунта", registrationPage.getTextErrorNameAccount());
        Assertions.assertEquals("Укажите пароль", registrationPage.getTextErrorPassword());
    }

    @Test
    public void registrationWithDOBInFuture() {
        registrationPage = new RegistrationPage(driver);
        registrationPage.setBirthDayInFuture();
        registrationPage.submitRegistration();
        Assertions.assertEquals("Укажите имя", registrationPage.getTextErrorFirstName());
        Assertions.assertEquals("Укажите фамилию", registrationPage.getTextErrorLastName());
        Assertions.assertEquals("Машину времени еще не изобрели, Марти, выбери другую дату", registrationPage.getTextErrorFutureDOB());
        Assertions.assertEquals("Укажите ваш пол", registrationPage.getTextErrorSex());
        Assertions.assertEquals("Укажите желаемое имя аккаунта", registrationPage.getTextErrorNameAccount());
        Assertions.assertEquals("Укажите пароль", registrationPage.getTextErrorPassword());
    }

    @Test
    public void registrationWithCyrillicNameEmail() {
        registrationPage = new RegistrationPage(driver);
        registrationPage.typeTextInNameAccountField("аккаунт");
        registrationPage.submitRegistration();
        Assertions.assertTrue(registrationPage.getTextErrorCyrillicNameAccount().contains("кирилл"));
    }

    @Test
    public void registrationWithAlreadyExistNameEmail() {
        registrationPage = new RegistrationPage(driver);
        registrationPage.typeTextInNameAccountField("wdriver.test");
        registrationPage.submitRegistration();
        Assertions.assertTrue(registrationPage.getTextErrorNameAccountAlreadyExist().contains("уже существует"));
    }

    @AfterEach
    private void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
