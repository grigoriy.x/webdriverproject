import Pages.HomePage;
import Pages.StartPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class LoginTest {

    WebDriver driver;
    StartPage startPage;
    HomePage homePage;

    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://mail.ru/");
    }

    @Test
    public void loginWithCorrectLoginAndPassword() {
        startPage = new StartPage(driver);
        startPage.typeUsername("wdriver.test");
        startPage.submitLogin();
        startPage.typePassword("wd3#test");
        startPage.submitLogin();
        homePage = new HomePage(driver);
        Assertions.assertEquals("wdriver.test@mail.ru", homePage.getTextUserEmailAfterLogin());
    }

    @Test
    public void loginWithLoginAndWithoutPassword() {
        startPage = new StartPage(driver);
        startPage.typeUsername("wdriver.test");
        startPage.submitLogin();
        startPage.typePassword("");
        startPage.submitLogin();
        Assertions.assertEquals("Введите пароль", startPage.getTextErrorMessage());
    }

    @Test
    public void loginWithoutLoginAndWithPassword() {
        startPage = new StartPage(driver);
        startPage.typeUsername("");
        startPage.submitLogin();
        Assertions.assertEquals("Введите имя ящика", startPage.getTextErrorMessage());
    }

    @Test
    public void loginWithCorrectLoginAndNonCorrectDomain() {
        startPage = new StartPage(driver);
        startPage.typeUsername("wdriver.test");
        startPage.selectMailDomain(1);
        startPage.submitLogin();
        Assertions.assertEquals("Неверное имя ящика", startPage.getTextErrorMessage());
    }

    @AfterEach
    private void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}