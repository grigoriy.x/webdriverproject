package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class RegistrationPage {

    WebDriver driver;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
    }

    By errorFirstName = By.xpath("//div[text()='Укажите имя']");
    By errorLastName = By.xpath("//div[text()='Укажите фамилию']");
    By errorDOB = By.xpath("//div[text()='Укажите дату рождения']");
    By errorFutureDOB = By.xpath("//div[text()='Машину времени еще не изобрели, Марти, выбери другую дату']");
    By errorSex = By.xpath("//div[text()='Укажите ваш пол']");
    By errorNameAccount = By.xpath("//div[text()='Укажите желаемое имя аккаунта']");
    By errorPassword = By.xpath("//div[text()='Укажите пароль']");
    By errorCyrillicNameAccount = By.xpath("//div[@data-text='В имени аккаунта нельзя использовать кириллицу']");
    By errorNameAccountAlreadyExist = By.xpath("//div[@class='b-form-field__errors__error js-invalid_exist js-error b-form-field__errors__error_visible']"); //в DOM существует два похожих div, один не видим

    By buttonRegister = By.xpath("//span[text()='Зарегистрироваться']");
    By dayOfBirthDayLocator = By.xpath("//span[text()='День']");
    By monthOfBirthDayLocator = By.xpath("//span[text()='Месяц']");
    By yearOfBirthDayLocator = By.xpath("//div[@class='b-date__year']");
    By lastNameFieldLocator = By.name("lastname");
    By nameAccountFieldLocator = By.xpath("//input[@data-blockid='email_name']");

    public RegistrationPage setBirthDayInFuture() {
        WebElement elementDay = driver.findElement(dayOfBirthDayLocator);
        elementDay.click();
        WebElement selectDay = driver.findElement(By.xpath("//span[text()='31']"));
        selectDay.click();
        WebElement elementMonth = driver.findElement(monthOfBirthDayLocator);
        elementMonth.click();
        WebElement selectMonth = driver.findElement(By.xpath("//span[text()='Декабрь']"));
        selectMonth.click();
        WebElement elementYear = driver.findElement(yearOfBirthDayLocator);
        elementYear.click();
        WebElement selectYear = driver.findElement(By.xpath("//span[text()='2019']"));
        selectYear.click();
        return this;
    }

    public String getTextErrorFirstName() {
        WebElement textErrorFirstName = driver.findElement(errorFirstName);
        return textErrorFirstName.getText();
    }

    public String getTextErrorLastName() {
        WebElement textErrorLastName = driver.findElement(errorLastName);
        return textErrorLastName.getText();
    }

    public String getTextErrorDOB() {
        WebElement textErrorDOB = driver.findElement(errorDOB);
        return textErrorDOB.getText();
    }

    public String getTextErrorFutureDOB() {
        WebElement textErrorFutureDOB = driver.findElement(errorFutureDOB);
        return textErrorFutureDOB.getText();
    }

    public String getTextErrorSex() {
        WebElement textErrorSex = driver.findElement(errorSex);
        return textErrorSex.getText();
    }

    public String getTextErrorNameAccount() {
        WebElement textErrorNameAccount = driver.findElement(errorNameAccount);
        return textErrorNameAccount.getText();
    }

    public String getTextErrorPassword() {
        WebElement textErrorPassword = driver.findElement(errorPassword);
        return textErrorPassword.getText();
    }

    public String getTextErrorCyrillicNameAccount() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(visibilityOfElementLocated(errorCyrillicNameAccount));
        WebElement textErrorCyrillicNameAccount = driver.findElement(errorCyrillicNameAccount);
        return textErrorCyrillicNameAccount.getText();
    }

    public void submitRegistration() {
        driver.findElement(buttonRegister).submit();
    }

    public RegistrationPage typeTextInNameAccountField(String nameAccount) {
        driver.findElement(nameAccountFieldLocator).sendKeys(nameAccount);
        return this;
    }

    public String getTextErrorNameAccountAlreadyExist() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(visibilityOfElementLocated(errorNameAccountAlreadyExist));
        WebElement textErrorNameAccountAlreadyExist = driver.findElement(errorNameAccountAlreadyExist);
        return textErrorNameAccountAlreadyExist.getText();
    }
}
