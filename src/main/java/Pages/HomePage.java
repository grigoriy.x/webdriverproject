package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class HomePage {

    WebDriver driver;

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    public String getTextUserEmailAfterLogin() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(visibilityOfElementLocated(By.xpath("//i[text()='wdriver.test@mail.ru']")));
        WebElement userEmail = driver.findElement(By.xpath("//i[text()='wdriver.test@mail.ru']"));
        return userEmail.getText();
    }

    public String getTextElementOfHeader() {
        WebElement elementHeader = driver.findElement(By.xpath("//span[text()='Письма']"));
        return elementHeader.getText();
    }
}
