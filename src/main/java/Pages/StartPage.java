package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class StartPage {

    WebDriver driver;

    By usernameLocator = By.id("mailbox:login");
    By passwordLocator = By.id("mailbox:password");
    By submitButtonLocator = By.id("mailbox:submit");
    By selectDomainLocator = By.id("mailbox:domain");
    By errorTextLocator = By.id("mailbox:error");


    public StartPage(WebDriver driver){
        this.driver = driver;
    }

    public StartPage typeUsername(String username) {
        driver.findElement(usernameLocator).sendKeys(username);
        return this;
    }

    public StartPage typePassword(String password) {
        driver.findElement(passwordLocator).sendKeys(password);
        return this;
    }

    public StartPage selectMailDomain(int variable) {
        Select domainSelect = new Select(driver.findElement(selectDomainLocator));
        domainSelect.selectByIndex(variable);
        return this;
    }

    public HomePage submitLogin() {
        driver.findElement(submitButtonLocator).submit();
        return new HomePage(driver);
    }

    public String getTextErrorMessage() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(visibilityOfElementLocated(errorTextLocator));
        WebElement errorText = driver.findElement(errorTextLocator);
        return errorText.getText();
    }

}
